# Uploader for BPMS artifacts to Nexus

## How to use
Checkout this project and copy upload-jars.sh and settings.xml to the unpacked offline maven repository. The script expects to be run inside the maven-repository folder, and may have unexpected behavior if run elsewhere.

1. Update environment settings.
    * Edit settings.xml to contain the credentials for the Nexus repository targeted for upload.
    * Edit upload-jars.sh to specify the correct Nexus repository URL, by default it assumes one running at localhost:8081.

2. Execute upload-jars.sh.
3. Check for a file called `artifacts` in the working directory. This contains an output of all artifacts uploaded along with packaging and GAV coordinates
