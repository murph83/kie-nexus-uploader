#!/bin/sh

NEXUS=http://localhost:8081/content/repositories/releases
VERSION="6.4.0.Final-redhat-3"
POMS=$(find . -name "*${VERSION}.pom")

rm artifacts
echo "Packaging  G:A:V" >> artifacts
for POM in $POMS; do
    STEM=`echo $POM | cut -d. -f1-8`
    JAR="$STEM.jar"
    SOURCE="$STEM-sources.jar"
    JAVADOC="$STEM-javadoc.jar"
    GROUP=$(echo $STEM | rev | cut -d/ -f4- | rev | cut -d/ -f2- | sed 's/\//\./g')
    ARTIFACT=$(echo $STEM | rev | cut -d/ -f1 | cut -d- -f4- | rev)

    MVN_CMD="mvn deploy:deploy-file -s settings.xml -Durl=$NEXUS"

    if [ -e $JAR ]; then
        MVN_CMD="$MVN_CMD -Dfile=$JAR"
        MVN_CMD="$MVN_CMD -DgeneratePom=false -Dpackaging=jar"
        PACKAGING=jar
    else
        MVN_CMD="$MVN_CMD -Dfile=$POM"
        PACKAGING=pom
    fi
    if [ -e $POM ]; then
        MVN_CMD="$MVN_CMD -DpomFile=$POM"
    fi
    if [ -e $SOURCE ]; then
        MVN_CMD="$MVN_CMD -Dsources=$SOURCE"
    fi
    if [ -e $JAVADOC ]; then
        MVN_CMD="$MVN_CMD -Djavadoc=$JAVADOC"
    fi

    eval $MVN_CMD
    echo "${PACKAGING}  ${GROUP}:${ARTIFACT}:${VERSION}" >> artifacts
done


